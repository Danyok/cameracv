################################################################################
##
## Copyright (C) 2022 Open Mobile Platform LLC.
## Contact: https://community.omprussia.ru/open-source
##
## This file is part of the Aurora OS Application Template project.
##
## Redistribution and use in source and binary forms,
## with or without modification, are permitted provided
## that the following conditions are met:
##
## * Redistributions of source code must retain the above copyright notice,
##   this list of conditions and the following disclaimer.
## * Redistributions in binary form must reproduce the above copyright notice,
##   this list of conditions and the following disclaimer
##   in the documentation and/or other materials provided with the distribution.
## * Neither the name of the copyright holder nor the names of its contributors
##   may be used to endorse or promote products derived from this software
##   without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
## THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
## FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
## IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
## FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
## OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
## PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
## LOSS OF USE, DATA, OR PROFITS;
## OR BUSINESS INTERRUPTION)
## HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
## WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
## EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
################################################################################

QT += opengl

TEMPLATE = app

TARGET = com.gitlab.danyok.cameracv

QT += multimedia core concurrent dbus

DEFINES += \
    DBUS_SERVICE=\\\"com.gitlab.danyok.cameracv\\\" \
    DBUS_PATH=\\\"com/gitlab/danyok/cameracv\\\" \
    DBUS_INTERFACE=\\\"com.gitlab.danyok.cameracv\\\" \

CONFIG += \
    sailfishapp \
    sailfishapp_i18n \

PKGCONFIG += \

INCLUDEPATH += \
    $$PWD/../libs/opencv/modules/core/include \
    $$PWD/../libs/opencv/modules/imgproc/include \
    $$PWD/../libs/opencv/modules/objdetect/include \
    $$OUT_PWD/../libs/opencv \

DEPENDPATH += \
    $$PWD/../libs/opencv/modules/core/include \
    $$PWD/../libs/opencv/modules/imgproc/include \
    $$PWD/../libs/opencv/modules/objdetect/include \
    $$OUT_PWD/../libs/opencv \

LIBS += \
    -L$$OUT_PWD/../libs/opencv/lib -lopencv_core -lopencv_imgproc -lopencv_objdetect -lopencv_calib3d -lopencv_features2d -lopencv_flann

TRANSLATIONS += \
    translations/com.gitlab.danyok.cameracv.ts \
    translations/com.gitlab.danyok.cameracv-ru.ts \

HEADERS += \
    src/qcvdetectfilter.h \

SOURCES += \
    src/main.cpp \
    src/qcvdetectfilter.cpp \

DISTFILES += \
    qml/icons/cameracv.svg \
    qml/cover/DefaultCoverPage.qml \
    qml/pages/MainPage.qml \
    qml/pages/AboutPage.qml \
    qml/cameracv.qml \
    translations/*.ts \
    com.gitlab.danyok.cameracv.desktop \

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

RESOURCES += \
    res.qrc
