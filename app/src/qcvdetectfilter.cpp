#include "qcvdetectfilter.h"

cv::CascadeClassifier classifier;

QVideoFilterRunnable* QCvDetectFilter::createFilterRunnable()
{
    return new QCvDetectFilterRunnable(this);
}

QImage QCvDetectFilterRunnable::QVideoFrameToQImage(QVideoFrame *videoFrame )
{
    if ( videoFrame->handleType() == QAbstractVideoBuffer::NoHandle )
    {
        QImage image(videoFrame->bits(),
                     videoFrame->width(),
                     videoFrame->height(),
                     QVideoFrame::imageFormatFromPixelFormat(videoFrame->pixelFormat()));
        if ( image.isNull() ) return QImage();
        if ( image.format() != QImage::Format_ARGB32 ) return image.convertToFormat( QImage::Format_ARGB32 );
        return image;
    }
    if ( videoFrame->handleType() == QAbstractVideoBuffer::GLTextureHandle )
    {
        QImage image( videoFrame->width(), videoFrame->height(), QImage::Format_ARGB32 );
        GLuint textureId = static_cast<GLuint>( videoFrame->handle().toInt() );
        QOpenGLContext* ctx = QOpenGLContext::currentContext();
        QOpenGLFunctions* f = ctx->functions();
        GLuint fbo;
        f->glGenFramebuffers( 1, &fbo );
        GLint prevFbo;
        f->glGetIntegerv( GL_FRAMEBUFFER_BINDING, &prevFbo );
        f->glBindFramebuffer( GL_FRAMEBUFFER, fbo );
        f->glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, textureId, 0 );
        f->glReadPixels( 0, 0,  videoFrame->width(),  videoFrame->height(), GL_RGBA, GL_UNSIGNED_BYTE, image.bits() );
        f->glBindFramebuffer( GL_FRAMEBUFFER, static_cast<GLuint>( prevFbo ) );
        return image.rgbSwapped();
    }
    return QImage();
}

QVideoFrame QCvDetectFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags)
{
    Q_UNUSED(flags);
    input->map(QAbstractVideoBuffer::ReadOnly);
//    qDebug() << "HandleType: " << surfaceFormat.handleType();

    if(surfaceFormat.handleType() == QAbstractVideoBuffer::NoHandle or surfaceFormat.handleType() == QAbstractVideoBuffer::GLTextureHandle)
    {
        QImage image = QVideoFrameToQImage(input);
        image = image.convertToFormat(QImage::Format_RGB888);
        cv::Mat mat(image.height(),
                         image.width(),
                         CV_8UC3,
                         image.bits(),
                         image.bytesPerLine());

        cv::flip(mat, mat, 0);

        if(classifier.empty())
        {
            QFile xml(":/res/faceclassifier.xml");
            if(xml.open(QFile::ReadOnly | QFile::Text))
            {
                QTemporaryFile temp;
                if(temp.open())
                {
                    temp.write(xml.readAll());
                    temp.close();
                    if(classifier.load(temp.fileName().toStdString()))
                    {
                        qDebug() << "Successfully loaded classifier!";
                    }
                    else
                    {
                        qDebug() << "Could not load classifier.";
                    }
                }
                else
                {
                    qDebug() << "Can't open temp file.";
                }
            }
            else
            {
                qDebug() << "Can't open XML.";
            }
        }
        else
        {
            std::vector<cv::Rect> detected;

            /*
             * Resize in not mandatory but it can speed up things quite a lot!
             */
            QSize resized = image.size().scaled(320, 240, Qt::KeepAspectRatio);
            cv::resize(mat, mat, cv::Size(resized.width(), resized.height()));

            classifier.detectMultiScale(mat, detected, 1.1);

            // We'll use only the first detection to make sure things are not slow on the qml side
            if(detected.size() > 0)
            {
                // Normalize x,y,w,h to values between 0..1 and send them to UI
                emit filter->objectDetected(float(detected[0].x) / float(mat.cols),
                        float(detected[0].y) / float(mat.rows),
                        float(detected[0].width) / float(mat.cols),
                        float(detected[0].height) / float(mat.rows));
                qDebug() << "Object detected!";
            }
            else
            {
                emit filter->objectDetected(0.0,
                                            0.0,
                                            0.0,
                                            0.0);
            }
        }
    }
    else
    {
        qDebug() << "Other surface formats are not supported yet!";
    }

    input->unmap();
    return *input;
}
