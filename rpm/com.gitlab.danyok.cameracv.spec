%define __provides_exclude_from ^%{_datadir}/%{name}/lib/.*$
%define __requires_exclude ^libopencv.*$

Name:       com.gitlab.danyok.cameracv
Summary:    Camera with OpenCV library for Aurora OS.
Version:    0.1
Release:    1
Group:      Qt/Qt
License:    BSD-3-Clause
Source0:    %{name}-%{version}.tar.bz2
BuildRequires:  pkgconfig(sailfishapp) >= 1.0.2
BuildRequires:  pkgconfig(protobuf)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Concurrent)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Multimedia)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5OpenGL)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  cmake >= 2.6.3
Requires:   sailfishsilica-qt5 >= 0.10.9

%description
Aurora OS Application Template.

%prep
%autosetup

%build
%qmake5 -r OPENCV_MFLAGS=%{?_smp_mflags}
make %{?_smp_mflags}

%install
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png

